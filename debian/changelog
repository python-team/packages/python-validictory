python-validictory (1.1.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Apply multi-arch hints. + python-validictory-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 17:56:16 +0100

python-validictory (1.1.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Thomas Goirand ]
  * Team upload.
  * Added patch from Steve Langasek to fix Python 3.10 compat
    (Closes: #1001480).

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Jan 2022 16:42:53 +0100

python-validictory (1.1.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Thomas Goirand ]
  * Team upload.
  * New upstream release.
  * Added python3-pytest as build-depends.
  * Removed version of python3-sphinx and python3-all build-depends, already
    satisfied even in oldoldstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Sep 2021 11:56:55 +0200

python-validictory (0.8.3-4) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Use Python 3 for building docs.
  * Drop Python 2 support.
  * Use pybuild for building package.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Enable autopkgtest-pkg-python testsuite.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 11:57:56 +0200

python-validictory (0.8.3-3) unstable; urgency=medium

  [ Chris Lamb ]
  * Team upload.
  * Move away from deprecated githubredir in debian/watch.
  * Bump Standards-Version to 4.2.1.
  * wrap-and-sort -sa.
  * Bump debhelper compat level to 11.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

 -- Chris Lamb <lamby@debian.org>  Tue, 18 Sep 2018 18:29:18 +0100

python-validictory (0.8.3-2) unstable; urgency=low

  * Updating the copyright, had incorect license information.
  * Updating the watch file, had a bad path to the tarballs.

 -- Paul Tagliamonte <paultag@debian.org>  Mon, 15 Oct 2012 21:35:51 -0400

python-validictory (0.8.3-1) unstable; urgency=low

  * Initial packaging.

 -- Paul Tagliamonte <paultag@debian.org>  Mon, 15 Oct 2012 21:14:53 -0400
